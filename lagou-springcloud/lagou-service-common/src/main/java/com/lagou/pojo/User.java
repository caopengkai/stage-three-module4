package com.lagou.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * 用户实体类
 * @author yzh
 * @date 2020/5/1 16:12
 */
@Data
@Entity
@Table(name="lagou_user")
public class User {

    /**
     * 自增主键
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
