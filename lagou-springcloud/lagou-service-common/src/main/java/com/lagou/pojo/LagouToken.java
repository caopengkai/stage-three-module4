package com.lagou.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name="lagou_token")
public class LagouToken {

    @Id
    private int id;
    private String email;
    private String token;
}
