package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EntityScan("com.lagou.pojo")
@EnableDiscoveryClient
@EnableFeignClients
public class CodeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeServiceApplication.class,args);
    }
}
