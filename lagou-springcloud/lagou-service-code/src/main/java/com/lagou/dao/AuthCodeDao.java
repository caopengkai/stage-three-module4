package com.lagou.dao;

import com.lagou.pojo.LagouAuthCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 验证码表数据层接口
 *
 */
public interface AuthCodeDao extends JpaRepository<LagouAuthCode, Long> {

    /**
     * 用原生SQL查询验证码集合
      * @param email 邮箱地址
     * @param code 验证码
     * @return
     */
    @Query(value = "select * from lagou_auth_code " +
                   "where expiretime > sysdate() and email = ?1 and code = ?2 " +
                   "order by createtime desc limit 1", nativeQuery = true)
    List<LagouAuthCode> findAllBySql(String email, String code);
}
