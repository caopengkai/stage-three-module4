package com.lagou.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@Component
public class EmailFeignClientFallBack implements EmailFeignClient {
    @Override
    public boolean sendEmail(String email, String code) {
        return false;
    }
}
