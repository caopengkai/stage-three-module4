package com.lagou.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="lagou-service-email", fallback = EmailFeignClientFallBack.class)
public interface EmailFeignClient {
    @GetMapping("/api/email/sendEmail/{email}/{code}")
    boolean sendEmail(@PathVariable("email") String email, @PathVariable("code") String code);
}
