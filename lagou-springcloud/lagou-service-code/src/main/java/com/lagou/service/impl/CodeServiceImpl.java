package com.lagou.service.impl;

import com.lagou.dao.AuthCodeDao;
import com.lagou.pojo.LagouAuthCode;
import com.lagou.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 验证码微服务业务层实现类
 *
 */
@Service
@Transactional
public class CodeServiceImpl implements CodeService {

    @Autowired
    private AuthCodeDao authCodeDao;

    @Override
    public void create(LagouAuthCode authCode) {
        authCodeDao.save(authCode);
    }

    @Override
    public boolean checkAuthCode(String email, String code) {
        List<LagouAuthCode> authCodeList = authCodeDao.findAllBySql(email, code);
        if(authCodeList != null && authCodeList.size() == 1) {
            return true;
        }
        return false;
    }
}
