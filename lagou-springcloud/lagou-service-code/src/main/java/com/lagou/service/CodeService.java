package com.lagou.service;


import com.lagou.pojo.LagouAuthCode;

/**
 * 验证码微服务业务层接口
 * @author
 * @date 2020/5/2 19:19
 */
public interface CodeService {

    /**
     * 保存验证码的方法
     * @param authCode 验证码实体对象
     */
    void create(LagouAuthCode authCode);

    /**
     * 校验验证码的方法
     * @param email 邮箱地址
     * @param code 验证码
     * @return 校验结果
     */
    boolean checkAuthCode(String email, String code);
}
