package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class LagouEurekaServerApp8762 {
    public static void main(String[] args) {
        SpringApplication.run(LagouEurekaServerApp8762.class,args);
    }
}
