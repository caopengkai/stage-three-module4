package com.lagou.service.impl;

import com.lagou.dao.TokenDao;
import com.lagou.dao.UserDao;
import com.lagou.pojo.LagouToken;
import com.lagou.pojo.User;
import com.lagou.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户微服务业务层实现类
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private TokenDao tokenDao;

    @Override
    public void saveUser(User user) {
        userDao.save(user);
    }

    @Override
    public User getUserByCondition(User user) {
        Example<User> example = Example.of(user);
        List<User> userList = userDao.findAll(example);
        if(userList != null && userList.size() > 0) {
            return userList.get(0);
        }
        return null;
    }

    @Override
    public void saveToken(LagouToken token) {
        tokenDao.save(token);
    }

    @Override
    public String info(LagouToken token) {
        Example<LagouToken> example = Example.of(token);
        List<LagouToken> tokenList = tokenDao.findAll(example);
        if(tokenList != null && tokenList.size() > 0) {
            return tokenList.get(0).getEmail();
        }
        return null;
    }
}
