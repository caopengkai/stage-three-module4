package com.lagou.service;

import com.lagou.pojo.LagouToken;
import com.lagou.pojo.User;

/**
 * 用户微服务业务层接口
 *
 */
public interface UserService {

    /**
     * 保存用户的方法
     * @param user 用户实体对象
     */
    void saveUser(User user);

    /**
     * 根据条件获得用户的方法
     * @param user 用户实体对象
     * @return
     */
    User getUserByCondition(User user);

    /**
     * 保存Token的方法
     * @param token 令牌实体对象
     * @return
     */
    void saveToken(LagouToken token);

    /**
     * 根据Token查询⽤户邮箱地址的方法
     * @param token 令牌
     * @return 邮箱地址
     */
    String info(LagouToken token);
}
