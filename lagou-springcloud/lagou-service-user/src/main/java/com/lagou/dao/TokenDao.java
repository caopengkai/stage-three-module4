package com.lagou.dao;

import com.lagou.pojo.LagouToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 令牌表数据层接口
 *
 */
public interface TokenDao extends JpaRepository<LagouToken, Long> {
}
