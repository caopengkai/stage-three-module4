package com.lagou.dao;

import com.lagou.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 用户表数据层接口
 *
 */
public interface UserDao extends JpaRepository<User, Long> {
}
