package com.lagou.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 验证码微服务调用接口
 *
 */
@FeignClient(name="lagou-service-code", fallback = CodeFeginClientFallBack.class )
public interface CodeFeignClient {

    /**
     * 校验验证码的方法
     * @param email 邮箱地址
     * @param code 验证码
     * @return 校验结果
     */
    @GetMapping("/api/code/validate/{email}/{code}")
    public boolean validate(@PathVariable String email, @PathVariable String code);
}
