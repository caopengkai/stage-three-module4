package com.lagou.client;

import org.springframework.stereotype.Component;

/**
 * 验证码微服务调用接口服务降级类
 *
 */
@Component
public class CodeFeginClientFallBack implements CodeFeignClient {

    @Override
    public boolean validate(String email, String code) {
        return false;
    }
}
