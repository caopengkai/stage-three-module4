package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EntityScan("com.lagou.pojo")
@EnableDiscoveryClient
public class EmialServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmialServiceApplication.class);
    }
}
