package com.lagou.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 用户微服务调用接口
 *
 */
@FeignClient(name="lagou-service-user", fallback = UserFeginClientFallBack.class)
public interface UserFeignClient {

    /**
     * 根据Token查询⽤户邮箱地址的方法
     * @param token 令牌
     * @return 邮箱地址
     */
    @GetMapping("/api/user/info/{token}")
    String info(@PathVariable String token);
}
