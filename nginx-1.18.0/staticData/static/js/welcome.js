function start() {
    var tlContent = setTimelineContent();
    var tlGlobal = new TimelineMax();
    tlGlobal.play();
}
function setTimelineContent() {
    var tl = new TimelineMax();
    tl.set(document.querySelector('.underline'), {
        width: 0
    });
    tl.staggerFromTo(document.querySelectorAll('.letter'), 0.9, {
        'font-size': 0,
        alpha: 0
    }, {
        'font-size': '80px',
        alpha: 1,
        ease: Expo.easeInOut
    }, 0.08);
    tl.fromTo(document.querySelector('.underline'), 0.6, {
        width: 0,
        alpha: 0
    }, {
        width: 160,
        alpha: 1,
        ease: Sine.easeOut
    });
    return tl;
}
start();